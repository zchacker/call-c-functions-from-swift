//
//  ViewController.swift
//  TestCLib
//
//  Created by Ahmed Adm on 27/02/1442 AH.
//  Copyright © 1442 Ahmed Adm. All rights reserved.
//

import UIKit
import OpusKit

class ViewController: UIViewController {
    
    // this tutorial from youtube for c in swift
    // https://www.youtube.com/watch?v=SsqsRfvbJOI&t=395s
    
    var decoder: OpaquePointer!
    var encoder: OpaquePointer!
    var sampleRate: opus_int32 = 8000
    var numberOfChannels: Int32 = 1
    var packetSize: opus_int32 = 320
    var encodeBlockSize: opus_int32 = 160
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize(sampleRate: 48000, numberOfChannels: 1, packetSize: 2880, encodeBlockSize: 280)
    }

    public func initialize(sampleRate: opus_int32 = 8000, numberOfChannels: Int32 = 1, packetSize: opus_int32 = 320, encodeBlockSize: opus_int32 = 160) {
        
        // Store variables.
        self.sampleRate = sampleRate
        self.numberOfChannels = numberOfChannels
        self.packetSize = packetSize
        self.encodeBlockSize = encodeBlockSize
                
        // Create status var.
        var status = Int32(0)
        
        // Create decoder.
        decoder = opus_decoder_create(sampleRate, numberOfChannels, &status)
        if (status != OPUS_OK) {
            print("OpusKit - Something went wrong while creating opus decoder: \(opus_strerror(status))")
        }
        
        // Create encoder.
        encoder = opus_encoder_create(sampleRate, numberOfChannels, OPUS_APPLICATION_VOIP, &status)
        if (status != OPUS_OK) {
            print("OpusKit - Something went wrong while creating opus encoder: \(opus_strerror(status))")
        }
        
        var ctl:Int32 = set_ctl_vars(encoder, 8000)
        
        print("codedc init ctl: \(ctl) ")
    }
    
}

