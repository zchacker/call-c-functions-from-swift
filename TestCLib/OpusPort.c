//
//  OpusPort.c
//  TestCLib
//
//  Created by Ahmed Adm on 27/02/1442 AH.
//  Copyright © 1442 Ahmed Adm. All rights reserved.
//

#include "OpusPort.h"
#include <opus.h>


int set_ctl_vars(OpusEncoder *enc , int bitrate){
    
    int err;
    
    err = opus_encoder_ctl(enc, OPUS_SET_BITRATE(bitrate));
    if(err < 0){
        printf("failed to set bitrate: %s\n", opus_strerror(err));
        return 0;
    }
    
    err = opus_encoder_ctl(enc, OPUS_SET_BANDWIDTH(OPUS_BANDWIDTH_FULLBAND));
    if(err < 0){
        printf("faild to set bandwith: %s \n", opus_strerror(err));
        return 0;
    }
    
    err = opus_encoder_ctl(enc, OPUS_SET_EXPERT_FRAME_DURATION(OPUS_FRAMESIZE_60_MS));
    if(err < 0){
        printf("faild to set bandwith: %s \n", opus_strerror(err));
        return 0;
    }
    
    return 1;
}
