//
//  OpusPort.h
//  TestCLib
//
//  Created by Ahmed Adm on 27/02/1442 AH.
//  Copyright © 1442 Ahmed Adm. All rights reserved.
//

#ifndef OpusPort_h
#define OpusPort_h

#include <stdio.h>
#include <opus.h>

int set_ctl_vars(OpusEncoder *enc , int bitrate);

#endif /* OpusPort_h */
